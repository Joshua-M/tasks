Rails.application.routes.draw do
  devise_for :users
  devise_scope :user do
    authenticated :user do
      root 'tasks#index', as: :authenticated_root
      resources :tasks do
        member do
          post :next_status
          post :revert_status
        end
      end
      resources :sub_tasks do
        member do
          post :next_status
          post :revert_status
        end
      end
      resources :messages
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

end
