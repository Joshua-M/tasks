class WelcomeEmailJob < ApplicationJob
  queue_as :default

  def perform(user_id)
    mail = UserMailer.welcome(user_id)
    mail.deliver_now
  end
end
