class SubTask < ApplicationRecord
  belongs_to :tasks, optional: true
  has_many :status, as: :statusable

  after_create :set_initial_status
  before_destroy :check_status
  after_destroy :delete_status

  private
  def set_initial_status
    status.create(display: "Pending")
  end

  def check_status
    status.count <= 1
  end

  def delete_status
    status.destroy_all   
  end
end
