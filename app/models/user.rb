class User < ApplicationRecord
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :tasks, -> { order(created_at: :DESC)}
  has_many :messages

  after_create :send_welcome_mail

  private

  def send_welcome_mail
    WelcomeEmailJob.set(wait: 2.minutes).perform_later(id)
  end
end
