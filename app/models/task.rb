class Task < ApplicationRecord
  belongs_to :user
  has_many :status, as: :statusable
  has_many :sub_tasks
  accepts_nested_attributes_for :sub_tasks, reject_if: proc { |attributes| attributes['description'].blank? }

  after_create :set_initial_status

  private
  def set_initial_status
    status.create(display: "Pending")
  end
end
