class ApplicationMailer < ActionMailer::Base
  default from: 'mjademarana@gmail.com'
  layout 'mailer'
end
