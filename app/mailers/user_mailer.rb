class UserMailer < ApplicationMailer
  default from: 'mjademarana@gmail.com'

  def welcome(user_id)
    @user = User.find(user_id)
    @url  = 'http://localhost:3000'
    mail(to: @user.email, subject: 'Welcome to My Awesome Site')
  end
end
