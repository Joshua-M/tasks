class TasksController < ApplicationController
  before_action :set_task, only: [:edit, :update, :destroy]
  def index
    @pagy, @tasks = pagy(current_user.tasks.all.includes(
                          :status,
                          sub_tasks: [
                            :status
                          ]),
                        items: 10)
  end

  def create
    respond_to do |format|
      format.html { redirect_to authenticated_root_path,
                    notice: "Failed to create Task." } and return if
                    !current_user.tasks.create!(task_params)
      format.html { redirect_to authenticated_root_path, notice: "Task was successfully created." }
    end
  end

  def edit
    render json: @task.sub_tasks
  end

  def update
    respond_to do |format|
      format.html { redirect_to authenticated_root_path,
                    notice: "Failed to update Task." } and return if
                    !@task.update!(task_params)
      format.html { redirect_to authenticated_root_path, notice: "Task was successfully updated." }
    end
  end

  def destroy
  end

  private

  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task).permit(:description, sub_tasks_attributes: [:description, :id])
  end
end
