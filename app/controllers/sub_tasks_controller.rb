class SubTasksController < ApplicationController
  before_action :set_sub_task

  def destroy
    respond_to do |format|
      format.html { redirect_to authenticated_root_path,
                    notice: "Failed to delete Sub Task." } and return if
                    !@sub_task.destroy
      format.html { redirect_to authenticated_root_path, notice: "Task was successfully deleted." }
    end
  end

  def next_status
    respond_to do |format|
      format.html { redirect_to authenticated_root_path,
                    notice: "Failed to update Sub Task status." } and return if
                    !update_next_status(@sub_task)
      format.html { redirect_to authenticated_root_path, notice: "Task status updated." }
    end
  end

  def revert_status
    respond_to do |format|
      format.html { redirect_to authenticated_root_path,
                    notice: "Failed to update Sub Task status." } and return if
                    !update_previous_status(@sub_task)
      format.html { redirect_to authenticated_root_path, notice: "Task status updated." }
    end
  end

  private
  def set_sub_task
    @sub_task = SubTask.find(params[:id])
  end
end
