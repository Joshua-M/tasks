class MessagesController < ApplicationController

  def index
    @messages = Message.all
  end

  def create
    message = current_user.messages.create!(message_params)
    if message.save
      ActionCable.server.broadcast 'room_channel',
                          body: message.body
    end

  end

  private

  def message_params
    params.require(:message).permit(:body)
  end

end
