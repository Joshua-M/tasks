$(document).ready(function(){
  let sub_task_input = $('#sub_task_input').html()
  let sub_task_container = $('#sub_tasks_container')
  let task_modal = $('#task_modal')
  let task_description = $('#task_description')
  let task_form = $('#task_form')
  let update_field = $('#update_field')
  let sub_task_count = 0

  // append new sub task field  
  $(document).on('click', '#add_sub_task', function(){    
    let input = $(sub_task_input)
    sub_task_container.append(input)
    $(input).find('input').first().attr('name', `task[sub_tasks_attributes][${++sub_task_count}][description]`)
  })

  //edit task
  $(document).on('click', '.edit_task', function(){
    sub_task_container.html('')
    get_task($(this))
  })

  function get_task(task){
    let id = task.attr('data-id')
    let description = task.attr('data-description')
    $.ajax({
      method: "GET",
      url: `/tasks/${id}/edit`,
      dataType: 'json',
      success: function(data){
        sub_task_count = data.length - 1
        data.forEach( (sub_task, idx) => {
          let input = $(sub_task_input)
          sub_task_container.append(input)
          $(input).append(`<input type="hidden" name="task[sub_tasks_attributes][${idx}][id]" value="${sub_task.id}">`)
          $(input).find('input').first().val(sub_task.description)
          $(input).find('input').first().attr('name', `task[sub_tasks_attributes][${idx}][description]`)
        });

        task_form.attr('action', `/tasks/${id}`)
        update_field.prependTo(task_form)
        task_description.val(description)
        task_modal.modal('show')
      }
    })
  }

  task_modal.on('hidden.bs.modal', function () {
    task_form.attr('action', `/tasks`)
    update_field.remove()
    sub_task_container.html(sub_task_container)
    task_description.val('')
  });
})//ducument ready