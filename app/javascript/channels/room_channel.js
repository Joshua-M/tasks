import consumer from "./consumer"

consumer.subscriptions.create("RoomChannel", {
  connected() {
    // console.log("test")
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    $("#messages").append(`<div class="message"><p>${data.body}</p></div>`)
    $('#message').val('')
    // Called when there's incoming data on the websocket for this channel
  }
});
