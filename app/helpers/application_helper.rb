module ApplicationHelper
  include Pagy::Frontend

  def update_next_status(model)
    res = false
    display = model.status.last.display
    if display == "Pending"
      res = model.status.create(display: "Ongoing")
    elsif display == "Ongoing"
      res = model.status.create(display: "Done")
    end
    res
  end

  def update_previous_status(model)
    res = false
    display = model.status.last.display
    if display == "Ongoing"
      res = model.status.create(display: "Pending")
    elsif display == "Done"
      res = model.status.create(display: "Ongoing")
    end
    res
  end
end
