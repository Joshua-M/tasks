class CreateStatuses < ActiveRecord::Migration[6.1]
  def change
    create_table :statuses do |t|
      t.references :statusable, polymorphic: true, null: false
      t.string :display

      t.timestamps
    end
  end
end
